/**
 * 
 */

/**
 * @author Luciana
 *
 */
import javax.swing.JOptionPane;
public class Atividade03 {

	/**
	 * Ler dois vetores de n�meros inteiros, cada um com 5 posi��es. Crie um terceiro vetor onde cada valor � a soma dos valores contidos
	 * nas posi��es respectivas dos vetores originais. Imprima depois os tres vetores.
	 */
	public static void main(String[] args) {
		
		int v1[] = new int [5];
		int v2[] = new int [5];
		int v3[] = new int [5];
		int x;
		
		for(x=0; x<5; x++){
			v1[x] = Integer.parseInt(JOptionPane.showInputDialog("digite um numero para o vetor 1 na posi��o " + x));
			v2[x] = Integer.parseInt(JOptionPane.showInputDialog("digite um numero para o vetor 2 na posi��o " + x ));
			v3[x] = v1[x] + v2[x];
		}
		
		for(x=0;x<5;x++){
		
			JOptionPane.showMessageDialog(null, "os valores dos vetores �:" +v1[x] +"||" + v2[x]+ "||" +v3[x]);
			
		}
	
	}
	
}
